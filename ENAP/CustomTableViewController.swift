//
//  CustomTableViewController.swift
//  ENAP
//
//  Created by DSB Mobile on 20/08/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class CustomTableViewController:UITableViewController{
    override func viewDidLoad(){
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor.whiteColor()
        var menuBarButtonItem = UIBarButtonItem(image: UIImage(named:"Menu")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal), style: UIBarButtonItemStyle.Plain, target: self, action: "showMenu")
        navigationItem.leftBarButtonItem = menuBarButtonItem
    }
    func showMenu(){
        var principalViewCont = navigationController?.parentViewController as! PrincipalViewController
        principalViewCont.showMenu()
    }
}
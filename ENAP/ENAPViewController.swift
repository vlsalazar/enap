//
//  ENAPViewController.swift
//  ENAP
//
//  Created by DSB Mobile on 11/08/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class ENAPViewController:CustomViewController{
    var viewConts = [UIViewController]()
    let titulos = ["Nosotros", "Temas Transversales", "Cómo encontrarnos", "Qué hacemos"]
    var indexViewCont = 0
    @IBOutlet weak var contentView:UIView!
    @IBOutlet weak var tabBarView:UIView!
    override func viewDidLoad(){
        super.viewDidLoad()
        let nosotrosViewCont = storyboard!.instantiateViewControllerWithIdentifier("NosotrosViewCont") as! UIViewController
        let transversalesViewCont = storyboard!.instantiateViewControllerWithIdentifier("TemasTransViewCont") as! UIViewController
        let encuentranosViewCont = storyboard!.instantiateViewControllerWithIdentifier("EncontrarnosViewCont") as! UIViewController
        let hacemosViewCont = storyboard!.instantiateViewControllerWithIdentifier("HacemosViewCont") as! UIViewController
        viewConts.append(nosotrosViewCont)
        viewConts.append(transversalesViewCont)
        viewConts.append(encuentranosViewCont)
        viewConts.append(hacemosViewCont)
        loadViewCont()
    }
    func loadViewCont(){
        navigationItem.title = titulos[indexViewCont]
        let button = tabBarView.viewWithTag(indexViewCont + 1) as! UIButton
        button.setBackgroundImage(UIImage(named:"BotonSeleccionado"), forState:UIControlState.Normal)
        for view in contentView.subviews as! [UIView] {
            view.removeFromSuperview()
        }
        for viewCont in childViewControllers as! [UIViewController] {
            viewCont.removeFromParentViewController()
        }
        let viewContTemp = viewConts[indexViewCont]
        var viewContFrame = contentView.frame
        viewContFrame.origin.y = 0
        viewContTemp.view.frame = viewContFrame
        contentView.addSubview(viewContTemp.view)
        addChildViewController(viewContTemp)
    }
    @IBAction func tabBarButton(button:UIButton){
        let buttonTemp = tabBarView.viewWithTag(indexViewCont + 1) as! UIButton
        buttonTemp.setBackgroundImage(nil, forState:UIControlState.Normal)
        indexViewCont = button.tag - 1
        loadViewCont()
    }
}
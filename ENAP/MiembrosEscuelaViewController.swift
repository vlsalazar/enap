//
//  MiembrosEscuelaViewController.swift
//  ENAP
//
//  Created by victor salazar on 24/08/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class MiembrosEscuelaViewController:CustomViewController, UIScrollViewDelegate{
    @IBOutlet weak var scrollView:UIScrollView!
    lazy var orgImgView:UIImageView = {
        var img = UIImage(named: "Organigrama")
        return UIImageView(image: img)
    }()
    override func viewDidLoad(){
        super.viewDidLoad()
        scrollView.addSubview(orgImgView)
        scrollView.contentSize = orgImgView.frame.size
    }
    func centerScrollViewContents(){
        var boundsSize = scrollView.bounds.size
        var contentsFrame = orgImgView.frame
        if contentsFrame.size.width < boundsSize.width {
            contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0
        }else{
            contentsFrame.origin.x = 0
        }
        if contentsFrame.size.height < boundsSize.height {
            contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0
        }else{
            contentsFrame.origin.y = 0
        }
        orgImgView.frame = contentsFrame
    }
    func viewForZoomingInScrollView(scrollView:UIScrollView) -> UIView? {
        return orgImgView
    }
    func scrollViewDidZoom(scrollView: UIScrollView) {
        centerScrollViewContents()
    }
    override func viewDidLayoutSubviews(){
        scrollView.contentSize = orgImgView.frame.size
        var scrollViewFrame = scrollView.frame
        var scaleWidth = scrollViewFrame.size.width / scrollView.contentSize.width
        var scaleHeight = scrollViewFrame.size.height / scrollView.contentSize.height
        var minScale = min(scaleWidth, scaleHeight)
        scrollView.minimumZoomScale = minScale
        scrollView.maximumZoomScale = 1.0
        scrollView.zoomScale = minScale
        centerScrollViewContents()
    }
}
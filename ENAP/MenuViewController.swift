//
//  MenuViewController.swift
//  ENAP
//
//  Created by DSB Mobile on 11/08/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
let mostrando = "mostrando"
let noMostrar = "no mostrar"
let oculto = "oculto"
class MenuViewController:UIViewController, UITableViewDataSource, UITableViewDelegate{
    var opciones = [["imagen": "ProgFormacion", "titulo": "Programas de Formación", "tipo": oculto], ["imagen": "ProgCapacitacion", "titulo": "Programas de Capacitación", "tipo": oculto], ["imagen": "Monitoreo", "titulo": "Monitoreo y Gestión del Conocimiento", "tipo": oculto], ["imagen": "Miembros", "titulo": "Miembros de la escuela", "tipo": noMostrar], ["imagen": "Contactanos", "titulo": "Contáctanos", "tipo": noMostrar]]
    var indexSelected = -1
    var subopcionSeleccionada = -1
    @IBOutlet weak var menuTableView:UITableView!
    override func viewDidLoad(){
        super.viewDidLoad()
        menuTableView.estimatedRowHeight = 50
        menuTableView.rowHeight = UITableViewAutomaticDimension
    }
    //MARK: - IBAction Events
    @IBAction func dismiss(gesture:UITapGestureRecognizer){
        var principalViewCont = parentViewController as! PrincipalViewController
        principalViewCont.showMenu()
    }
    @IBAction func mostrarENAP(gesture:UITapGestureRecognizer){
        var principalViewCont = parentViewController as! PrincipalViewController
        principalViewCont.showOpcion("0")
    }
    //MARK: - UITableView Events
    func tableView(tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return opciones.count + (indexSelected > -1 && indexSelected < 3 ? 2 : 0)
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var ind = indexPath.row
        if indexSelected > -1 {
            if ind > indexSelected {
                if ind <= (indexSelected + 2) {
                    ind = ind - indexSelected
                    let cell = tableView.dequeueReusableCellWithIdentifier("subopcionCell", forIndexPath:indexPath) as! SubOpcionTableViewCell
                    cell.subopcionLbl.textColor = UIColor.blackColor()
                    cell.subopcionLbl.text = ind == 1 ? "Características Esenciales" : "Componentes"
                    return cell
                }else{
                    ind -= 2
                }
            }
        }
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath:indexPath) as! MenuTableViewCell
        let dic = opciones[ind]
        cell.iconoImgView.image = UIImage(named: String(format:"%@%@", arguments: [dic["imagen"]!, (dic["tipo"] == mostrando) ? "Sel" : ""]))
        cell.opcionLbl.text = dic["titulo"]
        cell.flechaImgView.hidden = dic["tipo"] == noMostrar
        return cell
    }
    func tableView(tableView:UITableView, didSelectRowAtIndexPath indexPath:NSIndexPath){
        if indexPath.row > indexSelected && indexPath.row <= (indexSelected + 2) && indexSelected > -1 && indexSelected < 3{
            if subopcionSeleccionada != -1{
                var prevCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: indexSelected + subopcionSeleccionada, inSection: 0)) as! SubOpcionTableViewCell
                prevCell.subopcionLbl.textColor = UIColor.blackColor()
            }
            var cell = tableView.cellForRowAtIndexPath(indexPath) as! SubOpcionTableViewCell
            cell.subopcionLbl.textColor = ToolBox.redColor()
            subopcionSeleccionada = indexPath.row - indexSelected
            var opcion = "\(indexSelected)-\(subopcionSeleccionada)"
            var principalViewCont = parentViewController as! PrincipalViewController
            principalViewCont.showOpcion(opcion)
            return
        }
        subopcionSeleccionada = -1;
        var prevIndexSelected = indexSelected
        indexSelected = indexPath.row
        if prevIndexSelected > -1{
            var dicOcultar = opciones[prevIndexSelected]
            if prevIndexSelected < 3 {
                dicOcultar["tipo"] = oculto
            }
            opciones[prevIndexSelected] = dicOcultar
            var cellOcultar = tableView.cellForRowAtIndexPath(NSIndexPath(forRow:prevIndexSelected, inSection:0)) as! MenuTableViewCell
            cellOcultar.opcionLbl.textColor = UIColor.blackColor()
            cellOcultar.iconoImgView.image = UIImage(named:dicOcultar["imagen"]!)
            if prevIndexSelected < 3 {
                cellOcultar.flechaImgView.image = UIImage(named:"Flecha")
                UIView.animateWithDuration(0.5, animations:{
                    cellOcultar.flechaImgView.transform = CGAffineTransformMakeRotation(0)
                })
            }
            if indexSelected > prevIndexSelected && prevIndexSelected < 3{
                indexSelected -= 2
            }
        }
        if indexSelected == prevIndexSelected && indexSelected < 3 {
            indexSelected = -1
        }else{
            var dicMostrar = opciones[indexSelected]
            var cellMostrar = tableView.cellForRowAtIndexPath(indexPath) as! MenuTableViewCell
            cellMostrar.opcionLbl.textColor = UIColor.redColor()
            cellMostrar.iconoImgView.image = UIImage(named: String(format:"%@Sel", arguments:[dicMostrar["imagen"]!]))
            if dicMostrar["tipo"] == noMostrar {
                var principalViewCont = parentViewController as! PrincipalViewController
                principalViewCont.showOpcion("\(indexSelected)")
            }else{
                dicMostrar["tipo"] = mostrando
                opciones[indexSelected] = dicMostrar
                cellMostrar.flechaImgView.image = UIImage(named:"FlechaSel")
                UIView.animateWithDuration(0.5, animations:{
                    cellMostrar.flechaImgView.transform = CGAffineTransformMakeRotation(3.1415)
                })
            }
        }
        tableView.beginUpdates()
        if prevIndexSelected > -1 && prevIndexSelected < 3{
            tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: prevIndexSelected + 1, inSection: 0), NSIndexPath(forRow: prevIndexSelected + 2, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Top)
        }
        if indexSelected > -1 && indexSelected < 3 {
            tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: indexSelected + 1, inSection: 0), NSIndexPath(forRow: indexSelected + 2, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Top)
        }
        tableView.endUpdates()
    }
    //MARK: - Auxiliar Methods
    func selectOption(option:Int){
        var newOption = option
        if indexSelected != -1 {
            if option == indexSelected {
                return
            }
            if newOption > indexSelected {
                newOption += 2
            }
        }
        tableView(menuTableView, didSelectRowAtIndexPath: NSIndexPath(forRow: newOption, inSection: 0))
    }
}
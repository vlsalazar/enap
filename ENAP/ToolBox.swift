//
//  ToolBox.swift
//  ENAP
//
//  Created by DSB Mobile on 14/08/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import Foundation
import UIKit
class ToolBox{
    class func redColor() -> UIColor{
        return UIColor(red: 215/255.0, green: 7/255.0, blue: 25/255.0, alpha: 1.0)
    }
}
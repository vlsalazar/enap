//
//  MenuTableViewCell.swift
//  ENAP
//
//  Created by DSB Mobile on 12/08/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class MenuTableViewCell:UITableViewCell{
    @IBOutlet weak var iconoImgView:UIImageView!
    @IBOutlet weak var opcionLbl:UILabel!
    @IBOutlet weak var flechaImgView:UIImageView!
}
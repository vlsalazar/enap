//
//  ProgForCompTableViewController.swift
//  ENAP
//
//  Created by victor salazar on 19/08/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class ProgForCompTableViewController:CustomTableViewController{
    var arrComponentes = [["titulo": "Programa Intensivo para Directivos - PID", "desc": "Diseñado para formar a profesionales del sector público y privado que desean asumir el compromiso de ejerecer sus funciones en las distintas entidades del Estado. El PID tiene como objectivo desarrollar y fortalecer las competencias directivas de los participantes, preparándolos para liderar la reforma de la Gestión Pública."],
        ["titulo": "Programa de Alta Gerencia - PAG", "desc": "Dirigido a Directivos o asesores de Directivos que se encuentren en servicio, orientado a la consolidación de destrezas adquiridas por la experiencia, basándose en cuatro ejes: Gestión Pública, gestión directiva, hablidades sociales para el trabajo directivo, y habilidades de gestión."],
        ["titulo": "Programa de Desarrollo Gerencial - PDG", "desc": "Orientado a desarrollar competencias gerenciales en servidores públicos con potencial directivo, que pueden encontrarse en puestos de coordinación de programas y proyectos. El PDG desarrolla habilidades para fortalecer la capacidad de toma de decisiones y otras habilidades directivas."],
        ["titulo": "Programa de Formación de Talentos - PFT", "desc": "Este programa tiene como objectivo atraer a profesionales de cualquier sector al servicio público, formándolos al más alto nivel. El proceso de formación incluye, gracias a convenios realizados con distintas instituciones, pasantías a nivel Local, Regional, Nacional e Internacional; las cuales se llevarán a cabo al finalizar cada semestre académico."]]
    var indexSelected = -1
    //MARK: - TableView Events
    override func viewDidLoad(){
        super.viewDidLoad()
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrComponentes.count + ((indexSelected == -1) ? 0 : 1)
    }
    override func tableView(tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 1
    }
    override func tableView(tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 1
    }
    override func tableView(tableView:UITableView, cellForRowAtIndexPath indexPath:NSIndexPath) -> UITableViewCell {
        var row = indexPath.row
        if indexSelected != -1 && (indexSelected + 1) == indexPath.row{
            let cell = tableView.dequeueReusableCellWithIdentifier("descCell", forIndexPath: indexPath) as! DescripcionTableViewCell
            var componente = arrComponentes[indexSelected]
            cell.descripcionLbl.text = componente["desc"]
            return cell
        }
        let cell = tableView.dequeueReusableCellWithIdentifier("tituloCell", forIndexPath: indexPath) as! MenuTableViewCell
        var componente = arrComponentes[indexPath.row]
        cell.opcionLbl.text = componente["titulo"]
        return cell
    }
    override func tableView(tableView:UITableView, didSelectRowAtIndexPath indexPath:NSIndexPath){
        if indexSelected != -1 && indexSelected + 1 == indexPath.row {
            return
        }
        var prevIndexSelected = indexSelected
        indexSelected = indexPath.row
        if prevIndexSelected > -1 {
            var cellOcultar = tableView.cellForRowAtIndexPath(NSIndexPath(forRow:prevIndexSelected, inSection:0)) as! MenuTableViewCell
            cellOcultar.opcionLbl.textColor = UIColor.blackColor()
            cellOcultar.flechaImgView.image = UIImage(named:"Flecha")
            UIView.animateWithDuration(0.5, animations:{
                cellOcultar.flechaImgView.transform = CGAffineTransformMakeRotation(0)
            })
            if indexSelected > prevIndexSelected {
                indexSelected -= 1
            }
        }
        if indexSelected == prevIndexSelected {
            indexSelected = -1
        }else{
            var cellMostrar = tableView.cellForRowAtIndexPath(indexPath) as! MenuTableViewCell
            cellMostrar.opcionLbl.textColor = UIColor.redColor()
            cellMostrar.flechaImgView.image = UIImage(named:"FlechaSel")
            UIView.animateWithDuration(0.5, animations:{
                cellMostrar.flechaImgView.transform = CGAffineTransformMakeRotation(3.1415)
            })
        }
        tableView.beginUpdates()
        if prevIndexSelected > -1 {
            tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: prevIndexSelected + 1, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Top)
        }
        if indexSelected > -1 {
            tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: indexSelected + 1, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Top)
        }
        tableView.endUpdates()
    }
}
//
//  PrincipalViewController.swift
//  ENAP
//
//  Created by DSB Mobile on 11/08/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class PrincipalViewController:UIViewController, UIGestureRecognizerDelegate{
    var menuViewCont:MenuViewController?
    var otherViewCont:UIViewController?
    var showingMenu = false
    var originalPoint = CGPointZero
    var presentingIndViewCont = "ENAPNavViewCont"
    @IBOutlet weak var containerView:UIView!
    @IBOutlet weak var menuContainerView:UIView!
    @IBOutlet weak var launchTempView:UIView!
    //MARK: - UIView
    override func viewDidLoad(){
        super.viewDidLoad()
        let panGesture = UIPanGestureRecognizer(target: self, action: "menuPanGesture:")
        panGesture.delegate = self
        view.addGestureRecognizer(panGesture)
        
        let delay = 3.0 * Double(NSEC_PER_SEC)
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        dispatch_after(time, dispatch_get_main_queue(), {
            self.dissmissLaunchTempView(self)
        })
    }
    override func prepareForSegue(segue:UIStoryboardSegue, sender:AnyObject?){
        if segue.identifier! == "menuSegue" {
            menuViewCont = segue.destinationViewController as? MenuViewController
        }else{
            otherViewCont = segue.destinationViewController as? UIViewController
        }
    }
    override func prefersStatusBarHidden() -> Bool {
        return false
    }
    //MARK: - IBAction
    @IBAction func dissmissLaunchTempView(AnyObject){
        self.launchTempView.hidden = true
    }
    //MARK: - Auxiliar
    func showMenu(){
        view.userInteractionEnabled = false
        showingMenu = !showingMenu
        if showingMenu {
            menuContainerView.transform = CGAffineTransformMakeTranslation(-self.view.frame.size.width, 0)
            menuContainerView.hidden = false
        }
        UIView.animateWithDuration(0.5, delay: 0.0, options: .CurveEaseInOut, animations:{
            if self.showingMenu {
                self.menuContainerView.transform = CGAffineTransformIdentity
            }else{
                self.menuContainerView.transform = CGAffineTransformMakeTranslation(-self.view.frame.size.width, 0)
            }
        }, completion:{(success) in
            self.view.userInteractionEnabled = true
            if !self.showingMenu {
                self.menuContainerView.hidden = true
            }
        })
    }
    func showOpcion(opcion:String){
        var indViewCont = ""
        var esCarac = false
        var esCompEsp = false
        var arrViewCont = [String]()
        var arrTitulos = [String]()
        var arrCompCellIdentifiers = [String]()
        if opcion == "0" {
            indViewCont = "ENAPNavViewCont"
        }else if opcion == "0-1" {
            indViewCont = "ProgForCaracViewCont"
            arrViewCont = ["ProgForCaracForCompViewCont", "ProgForCaracAprenViewCont", "ProgForCaracForIntViewCont"]
            esCarac = true
        }else if opcion == "0-2" {
            indViewCont = "ProgForCompViewCont"
        }else if opcion == "1-1" {
            indViewCont = "ProgCapCaracViewCont"
            arrViewCont = ["ProgCapCaracCursosEstrucViewCont", "ProgCapCaracCursosDisenViewCont"]
            esCarac = true
        }else if opcion == "1-2" {
            indViewCont = "ProgCapCompViewCont"
            esCompEsp = true
            arrTitulos = ["Cursos Generales", "Cursos Especializados"]
            arrCompCellIdentifiers = ["compCurGenCell", "compCurEspCell"]
        }else if opcion == "2-1" {
            indViewCont = "MonGesCaracViewCont"
            arrViewCont = ["MonGesCaracInvAplicViewCont", "MonGesCaracServInfViewCont", "MonGesCaracLabDesViewCont"]
            esCarac = true
        }else if opcion == "2-2" {
            indViewCont = "MonGesCompViewCont"
            esCompEsp = true
            arrTitulos = ["Monitoreo", "Gestión del Conocimiento"]
            arrCompCellIdentifiers = ["compMonCell", "compGesConCell"]
        }else if opcion == "3" {
            indViewCont = "MiemEscViewCont"
        }else if opcion == "4" {
            indViewCont = "ContacViewCont"
        }
        if presentingIndViewCont == indViewCont {
            showMenu()
            return
        }
        presentingIndViewCont = indViewCont
        otherViewCont!.view.removeFromSuperview()
        otherViewCont!.removeFromParentViewController()
        otherViewCont = storyboard?.instantiateViewControllerWithIdentifier(indViewCont) as? UIViewController
        if esCarac {
            let navCont = otherViewCont as! UINavigationController
            let firstViewCont = (navCont.viewControllers as! [UIViewController])[0] as! CaracteristicasViewController
            firstViewCont.arrViewContIdentifiers = arrViewCont
        }
        if esCompEsp {
            let navCont = otherViewCont as! UINavigationController
            let firstViewCont = (navCont.viewControllers as! [UIViewController])[0] as! CompsTableViewController
            firstViewCont.arrTitulos = arrTitulos
            firstViewCont.compCellIdentifers = arrCompCellIdentifiers
        }
        addChildViewController(otherViewCont!)
        containerView.addSubview(otherViewCont!.view)
        showMenu()
    }
    //MARK: - PanGesture
    func gestureRecognizerShouldBegin(gestureRecognizer:UIGestureRecognizer) -> Bool {
        if let panGesture = gestureRecognizer as? UIPanGestureRecognizer {
            let puntoToque = panGesture.locationInView(view)
            if (!showingMenu) && puntoToque.x > 80 {
                return false
            }
        }
        return true
    }
    func menuPanGesture(gesture:UIPanGestureRecognizer){
        let puntoToque = gesture.locationInView(view)
        if gesture.state == .Began {
            originalPoint = puntoToque
            if !showingMenu {
                menuContainerView.transform = CGAffineTransformMakeTranslation(-self.view.frame.size.width, 0)
                menuContainerView.hidden = false
            }
        }else if gesture.state == .Changed {
            var deltaX = menuContainerView.transform.tx + puntoToque.x - originalPoint.x
            if deltaX > 0 {
                deltaX = 0
            }
            menuContainerView.transform = CGAffineTransformMakeTranslation(deltaX, 0)
            originalPoint = puntoToque
        }else if gesture.state == .Ended {
            var translateX = menuContainerView.transform.tx
            UIView.animateWithDuration(0.25, animations: {
                if translateX < -self.view.frame.size.width/2.0 {
                    self.menuContainerView.transform = CGAffineTransformMakeTranslation(-self.view.frame.size.width, 0)
                }else{
                    self.menuContainerView.transform = CGAffineTransformMakeTranslation(0, 0)
                }
            }, completion: { (finish) -> Void in
                self.showingMenu = translateX > -self.view.frame.size.width/2.0
            })
        }
    }
}
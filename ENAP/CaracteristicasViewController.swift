//
//  ProgForCaracViewController.swift
//  ENAP
//
//  Created by DSB Mobile on 14/08/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class CaracteristicasViewController:CustomViewController{
    var buttonSelected:CustomButtonView?
    var arrViewContIdentifiers = [String]()
    @IBOutlet weak var bottomView:UIView!
    override func viewDidLoad(){
        super.viewDidLoad()
        buttonSelected = view.viewWithTag(10) as? CustomButtonView
        loadBottomView()
    }
    @IBAction func buttonSelected(button:CustomButtonView){
        if buttonSelected != button {
            if buttonSelected != nil {
                buttonSelected!.tituloLbl.textColor = UIColor.blackColor()
                buttonSelected!.abajoView.hidden = true
            }
            buttonSelected = button
            buttonSelected!.tituloLbl.textColor = ToolBox.redColor()
            buttonSelected!.abajoView.hidden = false
            loadBottomView()
        }
    }
    func loadBottomView(){
        for subView in bottomView.subviews as! [UIView] {
            subView.removeFromSuperview()
        }
        for childViewCont in childViewControllers as! [UIViewController] {
            childViewCont.removeFromParentViewController()
        }
        var ind = buttonSelected!.tag - 10
        var viewContIdentifier = arrViewContIdentifiers[ind]
        var viewCont = storyboard!.instantiateViewControllerWithIdentifier(viewContIdentifier) as! UIViewController
        var nuevoView = viewCont.view
        var nuevoFrame = nuevoView.frame
        nuevoFrame.size = bottomView.frame.size
        nuevoView.frame = nuevoFrame
        bottomView.addSubview(nuevoView)
        addChildViewController(viewCont)
    }
}
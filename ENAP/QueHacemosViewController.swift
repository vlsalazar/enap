//
//  QueHacemosViewController.swift
//  ENAP
//
//  Created by victor salazar on 23/08/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class QueHacemosViewController:UIViewController{
    @IBAction func selectOption(control:UIControl){
        let principalViewCont = parentViewController!.navigationController!.parentViewController as! PrincipalViewController
        principalViewCont.menuViewCont?.selectOption(control.tag);
        principalViewCont.showMenu();
    }
}
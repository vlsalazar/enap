//
//  ProgCapCompTableViewController.swift
//  ENAP
//
//  Created by DSB Mobile on 20/08/15.
//  Copyright (c) 2015 Victor Salazar. All rights reserved.
//
import UIKit
class CompsTableViewController:CustomTableViewController{
    var arrTitulos = [String]()
    var compCellIdentifers = [String]()
    var indexSelected = -1
    override func viewDidLoad(){
        super.viewDidLoad()
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    //MARK: - Table view Events
    override func tableView(tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 1
    }
    override func tableView(tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 1
    }
    override func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return 2 + ((indexSelected == -1) ? 0 : 1)
    }
    override func tableView(tableView:UITableView, cellForRowAtIndexPath indexPath:NSIndexPath) -> UITableViewCell {
        var row = indexPath.row
        if indexSelected != -1 && (indexSelected + 1) == indexPath.row{
            let cell = tableView.dequeueReusableCellWithIdentifier(compCellIdentifers[indexSelected], forIndexPath: indexPath) as! UITableViewCell
            return cell
        }
        let cell = tableView.dequeueReusableCellWithIdentifier("tituloCell", forIndexPath: indexPath) as! MenuTableViewCell
        cell.opcionLbl.text = arrTitulos[indexPath.row]
        return cell
    }
    override func tableView(tableView:UITableView, didSelectRowAtIndexPath indexPath:NSIndexPath){
        if indexSelected != -1 && indexSelected + 1 == indexPath.row {
            return
        }
        var prevIndexSelected = indexSelected
        indexSelected = indexPath.row
        if prevIndexSelected > -1 {
            var cellOcultar = tableView.cellForRowAtIndexPath(NSIndexPath(forRow:prevIndexSelected, inSection:0)) as! MenuTableViewCell
            cellOcultar.opcionLbl.textColor = UIColor.blackColor()
            cellOcultar.flechaImgView.image = UIImage(named:"Flecha")
            UIView.animateWithDuration(0.5, animations:{
                cellOcultar.flechaImgView.transform = CGAffineTransformMakeRotation(0)
            })
            if indexSelected > prevIndexSelected {
                indexSelected -= 1
            }
        }
        if indexSelected == prevIndexSelected {
            indexSelected = -1
        }else{
            var cellMostrar = tableView.cellForRowAtIndexPath(indexPath) as! MenuTableViewCell
            cellMostrar.opcionLbl.textColor = UIColor.redColor()
            cellMostrar.flechaImgView.image = UIImage(named:"FlechaSel")
            UIView.animateWithDuration(0.5, animations:{
                cellMostrar.flechaImgView.transform = CGAffineTransformMakeRotation(3.1415)
            })
        }
        tableView.beginUpdates()
        if prevIndexSelected > -1 {
            tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: prevIndexSelected + 1, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Top)
        }
        if indexSelected > -1 {
            tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: indexSelected + 1, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Top)
        }
        tableView.endUpdates()
    }
}